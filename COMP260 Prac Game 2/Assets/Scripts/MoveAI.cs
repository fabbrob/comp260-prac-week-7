﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAI : MonoBehaviour {

    private Rigidbody rigidbody;
    public Rigidbody puck;

    public Transform puckStart;
    public Transform AIStartPosition;

    private Vector3 startingPosition;
    private Vector3 targetPosition;
    private Vector3 direction;
    private Vector3 velocity;

    public float speed;

    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
        startingPosition = rigidbody.position;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {
        startingPosition = rigidbody.position;

        if (puck.position.x > puckStart.position.x && (puck.position.z<3 && puck.position.z > -3))
        {
            targetPosition = puck.position;
        } else
        {
            targetPosition = AIStartPosition.position;
        }

        direction = targetPosition - startingPosition;
        velocity = direction.normalized * speed;

        float move = speed * Time.fixedDeltaTime;
        float distToTarget = direction.magnitude;
        if (move > distToTarget)
        {
            // scale the velocity down appropriately
            velocity = velocity * distToTarget / move;
        }

        rigidbody.velocity = velocity;
    }

}
