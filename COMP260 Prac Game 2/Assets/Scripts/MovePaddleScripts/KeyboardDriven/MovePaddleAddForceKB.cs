﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddleAddForceKB : MonoBehaviour
{
    //class variables
    private Rigidbody rigidbody;
    private Vector3 position;
    public string Horizontal;
    public string Vertical;
    public float force = 10f;

    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        position.x = Input.GetAxis(Horizontal);
        position.z = Input.GetAxis(Vertical);

        rigidbody.AddForce(position * force);
    }

    private Vector3 GetMousePosition()
    {
        // create a ray from the camera
        // passing through the mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // find out where the ray intersects the XZ plane
        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

    void OnDrawGizmos()
    {
        // draw the mouse ray
        Gizmos.color = Color.yellow;
        Vector3 pos = GetMousePosition();
        Gizmos.DrawLine(Camera.main.transform.position, pos);
    }

}
