﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour
{

    public AudioClip wallCollideClip;
    public AudioClip paddleCollideClip;
    public LayerMask paddleLayer;
    private AudioSource audio;
    public Transform startingPos;
    private Rigidbody rigidbody;


    // Use this for initialization
    void Start()
    {
        audio = GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody>();
        ResetPosition();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision collision)
    {
        // check what we have hit
        if (paddleLayer.Contains(collision.gameObject))
        {
            // hit the paddle
            audio.PlayOneShot(paddleCollideClip);
        }
        else
        {
            // hit something else
            audio.PlayOneShot(wallCollideClip);
        }
        audio.PlayOneShot(wallCollideClip);
    }

    public void ResetPosition()
    {
        // teleport to the starting position
        rigidbody.MovePosition(startingPos.position);
        //stop it from moving
        rigidbody.velocity = Vector3.zero;

    }

}